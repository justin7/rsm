<!DOCTYPE html>
<html>
 <?php include('user-head-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('user-header.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('user-side-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('user-page-header.php');?>

          <!-- ------------------------------------------------------------------ -->
          <div class="container user-task-cant">
            <div class="row">
        <div class="col-6 col-md-4">
                <div data-toggle="modal" data-target="#user-task-expand_1" class="card" style="width: 20rem;">
          <div class="card-body">
          <h5 class="card-title">Task title</h5>
          <h6 class="card-subtitle mb-2 text-muted">Task subtitle</h6>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
          </div>
          </div>
      </div>
              <div class="col-6 col-md-4"><div class="card" style="width: 20rem;">
          <div class="card-body">
          <h5 class="card-title">Task title</h5>
          <h6 class="card-subtitle mb-2 text-muted">Task subtitle</h6>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
          </div>
          </div></div>
              <div class="col-6 col-md-4"><div class="card" style="width: 20rem;">
          <div class="card-body">
          <h5 class="card-title">Task title</h5>
          <h6 class="card-subtitle mb-2 text-muted">Task subtitle</h6>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    
          </div>
          </div></div>
              
            </div>
            
          </div>



              <div class="modal fade" id="user-task-expand_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
              </button>
      </div>
      <div class="modal-body">
       <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
       <form>
         <div class="form-group">
          <label>comment</label>
          <textarea class="form-control"  rows="3"></textarea>
        </div>
            <div class="form-group">
         <input type="submit" value="Submit" class="btn btn-primary">
          </div>
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i>
        </button>

        <button type="button" class="btn btn-warning btn-circle"><i class="fa fa-link"></i>
        </button>

        <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-times"></i>
        </button>
                

        
                  
              </div>
            </div>
  </div>
</div>
         
 























        
          <?php include('user-footer.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('user-JS-files.php');?>
  </body>
</html>