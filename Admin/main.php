<?php 
session_start();
//$username=$_SESSION['username'];

  $username=$_SESSION['username'];
  // echo 'welcome:' .$username . '<br>';

  if(!isset($_SESSION['username'])) { //if not yet logged in
    header("Location: login.php");// send to login page
    exit;
  }

// if(!isset($_SESSION['username'])) { //if not yet logged in
//      header("Location: login.php");// send to login page
      // exit;
// }
?>
<!DOCTYPE html>
<html>
 <?php include('head-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('header.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('side-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('page-header.php');?>
          <!-- Dashboard Counts Section-->
          
          <!-- Dashboard Header Section    -->
         
          <!-- Projects Section-->
          
          <!-- Client Section-->
          
          <!-- Feeds Section-->
         
          <!-- Updates Section                                                -->
          
          <!-- Page Footer-->
          <?php include('footer.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('JS-files.php');?>
  </body>
</html>