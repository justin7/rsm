<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="../img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">Mark Stephen</h1>
              <p>Web Designer</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
                    <li class="active"><a href="main.php"> <i class="icon-user"></i>Profile</a></li>
                    <!-- <li><a href="add-emp.php"> <i class="icon-grid"></i>Add Employee</a></li>
                    <li><a href="view-emp.php"> <i class="icon-grid"></i>View Employee</a></li> -->
                   
                    <li class="parent "><a data-toggle="collapse" href="#emp-sub-item-1">
        <em class="fa fa-user">&nbsp;</em>Employee <span data-toggle="collapse" href="#emp-sub-item-1" class="icon pull-right"><em class=""></em></span>
        </a>
        <ul class="children collapse" id="emp-sub-item-1">
          <li><a class="" href="add-emp.php">
            <span class="fa fa-fas fa-user-plus">&nbsp;</span> Add Employee
          </a></li>
          <li><a class="" href="view-emp.php">
            <span class="fa fa-fas fa-eye">&nbsp;</span> View Employee
          </a></li>
          
        </ul>
      </li>

                    <li class="parent "><a data-toggle="collapse" href="#emp-sub-item-2">
        <em class="fa fa-navicon">&nbsp;</em> Repots <span data-toggle="collapse" href="#emp-sub-item-2" class="icon pull-right"><em class=""></em></span>
        </a>
        <ul class="children collapse" id="emp-sub-item-2">
          <li><a class="" href="task.php">
            <span class="fa fa-fas fa-files-o">&nbsp;</span> Upload Task
          </a></li>
          <li><a class="" href="reports.php">
            <span class="fa fa-fas fa-eye">&nbsp;</span> View Reports
          </a></li>
          
        </ul>
      </li>




                   <!--  <li><a href="#"> <i class="fa fa-bar-chart"></i>Charts </a></li>
                    <li><a href="#"> <i class="icon-padnote"></i>Forms </a></li>
                    <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
                      <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="#">Page</a></li>
                        <li><a href="#">Page</a></li>
                        <li><a href="#">Page</a></li>
                      </ul>
                    </li>
                    <li><a href="login.php"> <i class="icon-interface-windows"></i>Login page </a></li> -->
          </ul>
         <!--  <span class="heading">Extras</span>
          <ul class="list-unstyled">
            <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
          </ul> -->
        </nav>