
<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$mydb= "report";
$email="";
// $_SESSION["abc"]=123;
// Create connection
$conn = new mysqli($servername, $username, $password,$mydb);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);

} 
if (isset($_POST['submit'])){

 $username = mysqli_real_escape_string($conn, $_POST['username']);  
 $password = mysqli_real_escape_string($conn, $_POST['password']); 
  $mypassword=md5($_POST['password']); 
  // $user=$_POST['username'];
 $hashedpassword=password_hash("$mypassword",PASSWORD_DEFAULT);
 // echo password_hash("",PASSWORD_DEFAULT);
   // $mypassword=md5($_POST['mypassword']); 

 // $password="koustav";
 //  $hashedpassword=password_hash("koustav",PASSWORD_DEFAULT);
 //  echo password_hash("koustav",PASSWORD_DEFAULT);
 //  echo "<br>";
 // var_dump( password_verify($password,$hashedpassword));
  $sql = "SELECT * FROM admin WHERE username ='$username' AND password='$password'";
  $query=mysqli_query($conn,$sql);
  $sqli = "SELECT * FROM userlog WHERE username='$username' AND password='$password'";
  $queryi=mysqli_query($conn,$sqli);
   if(mysqli_num_rows($query)>0)
   {
      $_SESSION['username']=$username;
      header('location:main.php');

    } else if(mysqli_num_rows($queryi)>0) {
      $row = mysqli_fetch_assoc($queryi);
        $user_id = $row['id'];
         $_SESSION['username']=$username;
         header('location:../User/user-profile.php?id='.$row["id"].'');
      } 
    
  $conn->close();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Alchmi-login-page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">

   <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>ALCHMI</h1>
                  </div>
                  <p>STRATEGY. MARKETING. GROWTH</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form method="post" class="form-validate" action="">

                    <div class="form-group">
                      <input id="login-username" type="text" name="username" required data-msg="Please enter your username" class="input-material">
                      <label for="login-username" class="label-material" name="username" >User Name</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                      <label for="login-password" class="label-material" name="password">Password</label>
                    </div>
                    <!-- <a id="login" href="login.php" class="btn btn-info" name="login" type="submit">Login</a> -->
                    <button class="btn btn-info"  name="submit">Login</button>
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                  </form>

                  <a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        
        
       
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="../vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Main File-->
    <script src="../js/front.js"></script>
  </body>
</html>

