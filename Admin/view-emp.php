
<!DOCTYPE html>
<html>
 <?php 
 session_start();
 include('head-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('header.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('side-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('page-header.php');?>
          <!-- Dashboard Counts Section-->
          
          <!-- Dashboard Header Section    -->
          
          <!-- Projects Section-->
              
 <section class="tables">   
            <div class="container-fluid">
              <div class="row">

                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Employee List</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">

                      <?php

                      if(isset($_SESSION['msg'])){
                      echo $_SESSION['msg'];
                      }
                      unset($_SESSION['msg']);
                      include('dbconnection.php');
                      echo '<table class="table">
                      <table class="table" style="">
                      <thead>
                      <tr>
                      <th></th>
                      <th>Employee Name</th>
                      <th>Email</th>
                      <th>D.O.B</th>
                      <th>Mobile Number</th>
                      <th>Designation</th>
                      <th>Action</th>
                      <th>&nbsp</th>
                      <th>&nbsp</th>
                      <th>&nbsp</th>
                      <th>&nbsp</th>
                      </tr>
                      </thead>';

                       $sql = "SELECT * FROM userlog";
                       $result = $conn->query($sql);

                      if ($result->num_rows > 0) {

                        echo "";
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                              // echo "password: " . $row["password"]. "  email: " . $row["email"]. " designation: " . $row["designation"]. "<br>";
                              // $pass= $row["password"];
                              // $email=$row["email"];
                              // $designation=$row["designation"];

                            echo '<tr>
                           <th scope="row"></th>
                           <td><p>'.$row["username"].'</p></td>  
                           <td><p>'.$row["email"].'</p></td>  
                           <td><p>'.$row["dateofbirth"].'</p></td>  
                           <td><p>'.$row["mobileno"].'</p></td>  
                           <td><p>'.$row["designation"].'</p></td>  
                           <td><a href="/RSM/User/user-profile.php?id='.$row["id"].'"><button type="submit" value="Signin" class="btn btn-primary">Profile</button></a>
                           <td><button type="button" data-toggle="modal"id='.$row["id"].'  onClick="reply_click(this.id)" data-target="#admin_add_task" class="btn    btn-primary">Add Task</button></td>
                           <td><a href=""><button type="submit" value="Signin" class="btn btn-primary">View Task</button></a></td>
                          </tr>';
                          }

                            echo '</table>';
                           
                         } else {
                          echo "0 results";
                        }
                      $conn->close();
                      ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <?php
          include('dbconnection.php');
          // $user_id = $_GET['id'];
          // $user_id= isset($_GET['id']) ? $_GET['id'] : '';
          
          $sql = "SELECT * FROM task INNER JOIN userlog ON task.user_id=userlog.id ";
          $query=mysqli_query($conn,$sql);
          if(mysqli_num_rows($query)>0)
          {
          while($row=mysqli_fetch_array($query))
          {
             // echo $row["user_id"];
          }      
            
          }
          // $row = $result->fetch_assoc();
          // echo "$user_id";
          
          // if (isset($_POST['submit']))
          // {
          // $user_id= mysqli_real_escape_string($conn, $_POST['user_id']);
            
                  if(isset($_FILES['image'])){
                  $error = array();
                  $file_name = $_FILES['image']['name'];
                  $file_size = $_FILES['image']['size']; 
                  $file_tmp = $_FILES['image']['tmp_name'];
                  $file_size = $_FILES['image']['size'];
                  $user_id= mysqli_real_escape_string($conn, $_POST['user_id']);
                  $task_name = mysqli_real_escape_string($conn, $_POST['task_name']); 
                  $project_name = mysqli_real_escape_string($conn, $_POST['project_name']);
                  $task_description = mysqli_real_escape_string($conn, $_POST['task_description']); 
                  $file_ext = strtolower(end(explode('.', $file_name)));
                  $extensions  = array("jpeg","jpg","png");
                  $file_path = mysqli_real_escape_string($conn, $_POST['file_path']);

                  if (in_array($file_ext, $extensions) == false) {
                      $error[] = " please choose image extension has jpeg,jpg and png";
                  }
                  if (empty($error) == true) {
                      move_uploaded_file($file_tmp, "uploads/" . $file_name);
                      $file_path = $_SERVER['HTTP_REFERER']."uploads/" . $file_name;
                      $sql = "INSERT INTO  task(user_id,task_name, project_name,task_description,file_path) VALUES ('$user_id','$task_name', '$project_name', '$task_description','$file_path')";
                      $query = $conn->query($sql);
                      if ($query) 
                      {
                          echo 'image uploaded sucessfully';
                      }else{
                          echo 'Failed to upload';
                        }
                  }
                  }
      
          ?>
         

          <div id="admin_add_task" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">Assign Task</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                             
                              <form method="POST" action="" enctype="multipart/form-data">
                                <div class="form-group">
                                  <label>User ID</label>
                                  <input type="text" placeholder="" value="mytext" id="mytext" class="form-control" name="user_id" disabled="">

                                </div>
                                <div class="form-group">
                                  <label>Task Name</label>
                                  <input type="text" placeholder="Task Name"  name="task_name" class="form-control">
                                </div>
                                <div class="form-group">
                                  <label>Project Name</label>
                                  <input type="text" placeholder="Project Name"  name="project_name" class="form-control">
                                </div>
                                 <div class="form-group">
                                 <label>Task Description</label>
                                 <textarea class="form-control" name="task_description" rows="3"></textarea>
                                 </div>
                                <div class="form-group">
                                <label>Attach File</label>
                                 <input type="file"  name="image"   class="form-control-file" >
                                 </div>
                                <div class="form-group">       
                                  <input type="submit" nmae="submit" value="Submit" class="btn btn-primary">
                                </div>
                              </form>
                            </div>
                          
                          </div>
                        </div>
                      </div>
          
          <!-- Client Section-->
          
          <!-- Feeds Section-->
         
          <!-- Updates Section                                                
          
          <-- Page Footer-->
          <?php include('footer.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('JS-files.php');?>
    <script type="text/javascript">
function reply_click(clicked_id)
{
   // alert(clicked_id);
   var text = clicked_id;
   document.getElementById("mytext").value = text;  
}
</script>
  </body>
</html>

