
<!DOCTYPE html>
<html>
 <?php include('superAdminHead-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('superAdminHeader.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('superAdminSide-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('superAdminPage-header.php');?>
          <!-- Dashboard Counts Section-->
          
          <!-- Dashboard Header Section    -->
         
          <!-- Projects Section-->
          
          <!-- Client Section-->
          
          <!-- Feeds Section-->
         
          <!-- Updates Section                                                -->
          
          <!-- Page Footer-->
          <?php include('superAdminFooter.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('superAdminJS-files.php');?>
  </body>
</html>