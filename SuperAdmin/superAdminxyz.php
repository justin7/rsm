<!DOCTYPE html>
<html>
 <?php include('superAdminHead-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('superAdminHeader.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('superAdminSide-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('superAdminPage-header.php');?>



    <div class="container">
        
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
              <a href="#"><i class="icon-user sad-pro-icon"></i>
                <h3 class="sad-pro-font">Profile</h3></a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 center">
              <a href="superAdminAddAdmin.php"><i class="icon-user sad-pro-icon"></i>
               <h3 class="sad-pro-font">Add Admin</h3></a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
              <a href="superAdminViewAdmin.php"><i class="icon-user sad-pro-icon"></i><h3 class="sad-pro-font">View Admin</h3></a>
        </div>

        
      </div>
    </div>
          <!-- Page Footer-->
          <?php include('superAdminFooter.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('superAdminJS-files.php');?>
  </body>
</html>