
<!DOCTYPE html>
<html>
 <?php include('superAdminHead-links.php');?>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include('superAdminHeader.php');?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include('superAdminSide-navbar.php');?>
        <div class="content-inner">
          <!-- Page Header-->
         <?php include('superAdminPage-header.php');?>

         <br>
          <div class="container">
         <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add Admin</h3>
                    </div>
                    <div class="card-body">
                     <form method="post" action="">

                      <div class="form-group">
                          <label class="form-control-label">Name</label>
                          <input type="text" placeholder="Name" name="spname" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label class="form-control-label">Password</label>
                          <input type="password" placeholder="Password" name="password" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label class="form-control-label">Email</label>
                          <input type="email" placeholder="Email Address" name="email" class="form-control" required="">
                        </div>
                        <div class="form-group">       
                          <button class="btn btn-primary" name="submit" type="submit">Submit</button> 
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          <!-- Page Footer-->
          <?php include('superAdminFooter.php');?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include('superAdminJS-files.php');?>
  </body>
</html>